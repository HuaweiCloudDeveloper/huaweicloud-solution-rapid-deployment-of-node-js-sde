[TOC]

**解决方案介绍**
===============
该方案可以帮助您在华为云弹性云服务器中快速搭建JavaScript运行环境，一键实现Node.js的安装和配置。Node.js是一个基于Chrome V8 引擎的JavaScript 运行环境，其使用了一个事件驱动、非阻塞式 I/O 的模型，使其轻量又高效，是一款非常适合在分布式设备上运行数据密集型的实时应用。适用于Web应用、应用程序监控、媒体流、远程控制、桌面和移动应用等场景。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-node-js-sde.html

**架构图**
---------------
![架构图](./document/rapid-deployment-of-node-js-sde.PNG)

**架构描述**
---------------
该解决方案会部署如下资源：

- 创建1台弹性云服务器ECS，安装Node.js并完成相关配置，用个人网站的业务以及数据库节点。
- 创建1个弹性公网IP，并绑定到弹性云服务器，用于提供访问公网和被公网访问能力。
- 创建安全组，保护弹性云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口，保证个人网站安全。

**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-node-js-sde
├── rapid-deployment-of-node-js-sde.tf.json -- 资源编排模板
├── userdata
    ├── node_install.sh  -- 脚本配置文件
```
**开始使用**
---------------

1.登录华为云弹性云服务器ECS控制台，查看ECS基本信息并获取绑定的弹性公网EIP。

**图 1** 查看ECS绑定的EIP
![登录ECS云服务器控制平台](./document/readme-image-001.PNG)

2.登录服务器，验证node.js是否安装成功。

分别输入 node -v、npm -v查看版本信息

**图 2** 查看版本信息
![登录ECS云服务器控制平台](./document/readme-image-002.png)
