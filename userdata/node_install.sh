#!/bin/bash
wget https://nodejs.org/dist/v10.14.1/node-v10.14.1-linux-x64.tar.xz
tar xvJf node-v10.14.1-linux-x64.tar.xz -C /root
ln -s /root/node-v10.14.1-linux-x64/bin/node /usr/local/bin/node

ln -s /root/node-v10.14.1-linux-x64/bin/npm /usr/local/bin/npm

node -v
npm -v